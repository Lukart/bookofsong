package finalProject.entities;

import javax.persistence.*;

@Entity
@Table(name="AddNewSong")
public class AddNewSong {

    @Id
    @GeneratedValue
    private Integer id;

    private Integer categorie;
    private String title;
    private String text_of_verse;
    private String chords_of_verse;
    private Integer number_of_verse;
    private String user;


    public AddNewSong(Integer categorie, String title, String text_of_verse, String chords_of_verse) {
        this.categorie = categorie;
        this.title = title;
        this.text_of_verse = text_of_verse;
        this.chords_of_verse = chords_of_verse;
    }


    public AddNewSong() {
    }

    public Integer getId() {
        return id;
    }

    public Integer getCategorie() {
        return categorie;
    }

    public void setCategorie(Integer categorie) {
        this.categorie = categorie;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText_of_verse() {
        return text_of_verse;
    }

    public void setText_of_verse(String text_of_verse) {
        this.text_of_verse = text_of_verse;
    }

    public String getChords_of_verse() {
        return chords_of_verse;
    }

    public void setChords_of_verse(String chords_of_verse) {
        this.chords_of_verse = chords_of_verse;
    }

    public Integer getNumber_of_verse() {
        return number_of_verse;
    }

    public void setNumber_of_verse(Integer number_of_verse) {
        this.number_of_verse = number_of_verse;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        user = user;
    }
}
