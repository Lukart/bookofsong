package finalProject.reposiories;

import finalProject.entities.AddNewSong;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddRepository extends CrudRepository <AddNewSong, Integer> {
}
