package finalProject.reposiories;

import finalProject.entities.AddNewSong;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class AddRepositoryInstnce {

    private final AddRepository addRepository;

    public AddRepositoryInstnce(AddRepository addRepository) {
        this.addRepository = addRepository;
    }

    @Transactional
    public void addOneSong(Integer categorie, String title, String text_of_verse, String chords_of_verse, String number_of_verse) {
        AddNewSong addNewSong = new AddNewSong(categorie, title, text_of_verse, chords_of_verse);
        addRepository.save(addNewSong);
    }
}












//        em.createNativeQuery("INSERT INTO AddNewSong (categorie, title, text_of_verse, chords_of_verse, number_of_verse) " +
//                        "VALUES(:a, :b, :c, :d, :e)")
//                .setParameter("a",
//                .setParameter("b", objUser.getUpassword())
//                .setParameter("c", objUser.getEmail())
//                .setParameter("d", objUser.getMobile())
//                .setParameter("e", objUser.getFax()).executeUpdate();
//
//    }
//}
