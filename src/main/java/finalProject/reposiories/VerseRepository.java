package finalProject.reposiories;

import finalProject.entities.Verse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VerseRepository extends CrudRepository <Verse,Integer> {
}
